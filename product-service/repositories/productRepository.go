package repositories

import (
	"learning_micro/product-service/models"
	"log"

	"github.com/jmoiron/sqlx"
)

type ProductRepository interface {
	GetProduct() (*[]models.Product, error)
	CreateProduct(product *models.Product) error
	UpdateProduct(product *models.Product) (string, error)
	DeleteProduct(Id int) (string, error)
}

type productRepository struct {
	db *sqlx.DB
}

func NewProductRepository(db *sqlx.DB) *productRepository {
	return &productRepository{db}
}

func (r *productRepository) GetProduct() (*[]models.Product, error) {
	db := r.db
	var result = []models.Product{}
	sqlStatement := `SELECT * FROM products`
	rows, err := db.Query(sqlStatement)
	if err != nil {
		log.Printf("Error get data products with err: %s", err)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var product = models.Product{}

		err := rows.Scan(&product.Id, &product.ProductName, &product.CreatedAt, &product.UpdatedAt)
		if err != nil {
			log.Printf("Error get data product with err: %s", err)
			return nil, err
		}

		result = append(result, product)
	}

	return &result, nil
}

func (r *productRepository) CreateProduct(product *models.Product) error {
	db := r.db

	sqlStatement := `INSERT INTO products (product_name, created_at, updated_at) VALUES ($1, $2, $3)`
	_, err := db.Exec(sqlStatement, &product.ProductName, &product.CreatedAt, &product.UpdatedAt)
	if err != nil {
		log.Printf("Error insert products with err: %s", err)
		return err
	}

	return nil
}

func (r *productRepository) UpdateProduct(product *models.Product) (string, error) {
	db := r.db
	var product_name string
	sqlUpdate := `UPDATE products SET product_name = $2, updated_at = $3 WHERE id = $1 RETURNING product_name`
	err := db.QueryRowx(sqlUpdate, &product.Id, &product.ProductName, &product.UpdatedAt).Scan(&product_name)
	if err != nil {
		log.Printf("Error update product to database with err: %s", err)
		return product_name, err
	}
	return product_name, nil
}

func (r *productRepository) DeleteProduct(Id int) (string, error) {
	db := r.db
	var product_name string
	sqlDelete := `DELETE FROM products WHERE id = $1 RETURNING product_name`
	err := db.QueryRowx(sqlDelete, Id).Scan(&product_name)
	if err != nil {
		log.Printf("Error update product to database with err: %s", err)
		return product_name, err
	}

	return product_name, nil
}
