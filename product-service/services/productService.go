package services

import (
	"learning_micro/product-service/models"
	"learning_micro/product-service/repositories"
	"log"
	"time"
)

type ProductService interface {
	GetProduct() (*[]models.Product, error)
	CreateProduct(product *models.Product) error
	UpdateProduct(Id int, product *models.Product) (string, error)
	DeleteProduct(Id int) (string, error)
}

type productService struct {
	productRepository repositories.ProductRepository
}

func NewProductService(repository repositories.ProductRepository) *productService {
	return &productService{repository}
}

func (s *productService) GetProduct() (*[]models.Product, error) {
	products, err := s.productRepository.GetProduct()
	if err != nil {
		log.Printf("Error get data products with err: %s", err)
		return nil, err
	}

	return products, nil
}

func (s *productService) CreateProduct(product *models.Product) error {
	productCreate := models.Product{
		ProductName: product.ProductName,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}
	if err := s.productRepository.CreateProduct(&productCreate); err != nil {
		log.Printf("Error create product to database with err: %s", err)
		return err
	}

	return nil
}

func (s *productService) UpdateProduct(Id int, product *models.Product) (string, error) {
	productUpdate := models.Product{
		Id:          Id,
		ProductName: product.ProductName,
		UpdatedAt:   time.Now(),
	}

	product_name, err := s.productRepository.UpdateProduct(&productUpdate)
	if err != nil {
		log.Printf("Error update product to database with err: %s", err)
		return product_name, err
	}

	return product_name, nil
}

func (s *productService) DeleteProduct(Id int) (string, error) {
	product_name, err := s.productRepository.DeleteProduct(Id)
	if err != nil {
		log.Printf("Error delete product to database with err: %s", err)
		return product_name, err
	}

	return product_name, nil
}
