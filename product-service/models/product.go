package models

import "time"

type Product struct {
	Id          int       `json:"id"`
	ProductName string    `json:"product_name" validate:"required"`
	CreatedAt   time.Time `json:"created_at,omitempty"`
	UpdatedAt   time.Time `json:"updated_at,omitempty"`
}

type UserLogin struct {
	Id       int    `json:"id"`
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}
