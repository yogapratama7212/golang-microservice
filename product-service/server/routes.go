package server

import (
	"fmt"
	"learning_micro/product-service/controllers"
	"learning_micro/product-service/db"
	"learning_micro/product-service/migrations"
	"learning_micro/product-service/repositories"
	"learning_micro/product-service/services"
	"os"

	"github.com/gin-gonic/gin"
)

func Init() {
	db := db.Connect()
	defer db.Close()
	productRepo := repositories.NewProductRepository(db)
	productService := services.NewProductService(productRepo)
	productController := controllers.NewProductController(productService)
	gin.SetMode(gin.ReleaseMode)
	e := gin.New()
	e.Use(gin.Recovery())
	v1 := e.Group("/api/v1")
	{
		v1.POST("/migrate", migrations.Migrate)
		v1.POST("/login-product", productController.LoginController)
	}
	product := v1.Group("product")
	{
		product.GET("", productController.GetProductController)
		product.POST("", productController.CreateProductController)
		product.PUT("", productController.UpdateProductController)
		product.DELETE("", productController.DeleteProductController)
	}
	e.Run(fmt.Sprintf(":%s", os.Getenv("SERVER_PORT")))
}

// func RegisterProductRoute(r *gin.Engine, productController controllers.ProductController) {
// 	productRouter := r.Group("/api/v1/product")
// 	productRouter.GET("", productController.GetProductController)
// }
