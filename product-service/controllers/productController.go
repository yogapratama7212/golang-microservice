package controllers

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"learning_micro/product-service/helpers"
	"learning_micro/product-service/models"
	"learning_micro/product-service/services"
	"learning_micro/product-service/validations"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ProductController struct {
	service services.ProductService
}

func NewProductController(service services.ProductService) *ProductController {
	return &ProductController{service}
}

type User struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
	// Data    interface{} `json:"data,omitempty"`
	Data interface{} `json:"data"`
}

func (productController *ProductController) GetProductController(c *gin.Context) {
	baseUrl := fmt.Sprintf("http://%s/api/v1/get-user-id/", os.Getenv("AUTH_HOST"))
	resp, err := http.Get(baseUrl)
	if err != nil {
		log.Printf("Error get user id with: %s", err)
		helpers.NewHandlerResponse(err.Error(), nil).Failed(c)
	}
	defer resp.Body.Close()
	// body, err := ioutil.ReadAll(resp.Body)
	// if err != nil {
	// 	log.Printf("Error read user id with: %s", err)
	// 	helpers.NewHandlerResponse(err.Error(), nil).Failed(c)
	// }

	var result User
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		fmt.Println("Can not unmarshal JSON")
	}
	fmt.Println("USER ID  = ", result)

	products, err := productController.service.GetProduct()
	if err != nil {
		helpers.NewHandlerResponse(err.Error(), nil).Failed(c)
		return
	}

	helpers.NewHandlerResponse("Successfully get products", products).Success(c)
}

func (productController *ProductController) CreateProductController(c *gin.Context) {
	var product models.Product
	if err := c.ShouldBindJSON(&product); err != nil {
		helpers.NewHandlerResponse(err.Error(), nil).BadRequest(c)
		return
	}

	if err := validations.DoValidation(&product); err != nil {
		helpers.NewHandlerValidationResponse(err, nil).BadRequest(c)
		return
	}

	if err := productController.service.CreateProduct(&product); err != nil {
		helpers.NewHandlerResponse(err.Error(), nil).Failed(c)
		return
	}
	helpers.NewHandlerResponse("Successfully create product", nil).SuccessCreate(c)
}

func (productController *ProductController) UpdateProductController(c *gin.Context) {
	var product models.Product

	id, err := strconv.Atoi(c.Query("id"))
	if err != nil {
		if err == sql.ErrNoRows {
			helpers.NewHandlerResponse("Product Not Found", nil).Failed(c)
			return
		}
		helpers.NewHandlerResponse("Error convert data", nil).BadRequest(c)
		return
	}

	if err := c.ShouldBindJSON(&product); err != nil {
		helpers.NewHandlerResponse(err.Error(), nil).BadRequest(c)
		return
	}

	if err := validations.DoValidation(&product); err != nil {
		helpers.NewHandlerValidationResponse(err, nil).BadRequest(c)
		return
	}

	product_name, err := productController.service.UpdateProduct(id, &product)
	if err != nil {
		if err == sql.ErrNoRows {
			helpers.NewHandlerResponse("Product Not Found", nil).Failed(c)
			return
		}
		helpers.NewHandlerResponse(err.Error(), nil).Failed(c)
		return
	}
	helpers.NewHandlerResponse(fmt.Sprintf("Product name %s was updated to the database\n", product_name), nil).Success(c)
}

func (productController *ProductController) DeleteProductController(c *gin.Context) {
	id, err := strconv.Atoi(c.Query("id"))
	if err != nil {
		helpers.NewHandlerResponse("Error convert data", nil).BadRequest(c)
		return
	}

	product_name, err := productController.service.DeleteProduct(id)
	if err != nil {
		if err == sql.ErrNoRows {
			helpers.NewHandlerResponse("Product Not Found", nil).Failed(c)
			return
		}
		helpers.NewHandlerResponse(err.Error(), nil).Failed(c)
		return
	}
	helpers.NewHandlerResponse(fmt.Sprintf("Product name %s was deleted to the database\n", product_name), nil).Success(c)
}

func (productController *ProductController) LoginController(c *gin.Context) {
	var user models.UserLogin
	client := &http.Client{}
	user.Email = "yoga@gmail.com"
	user.Password = "Yoga"
	body, _ := json.Marshal(&user)
	sent := bytes.NewBuffer(body)
	req, err := http.NewRequest("POST", "http://auth:8001/api/v1/login", sent)
	if err != nil {
		panic(err)
	}
	_, err = client.Do(req)
	if err != nil {
		panic(err)
	}
	helpers.NewHandlerResponse("Sucessfully Login", nil).Success(c)
}
