package migrations

import (
	"learning_micro/product-service/db"
	"learning_micro/product-service/helpers"
	"log"

	"github.com/gin-gonic/gin"
)

func Migrate(c *gin.Context) {
	db := db.Connect()
	defer db.Close()

	sqlStatement := `CREATE TABLE IF NOT EXISTS products (
		id serial PRIMARY KEY,
		product_name varchar(255) NOT NULL,
		user_id int NOT NULL,
		created_at timestamp,
		updated_at timestamp
	);`

	_, err := db.Exec(sqlStatement)

	if err != nil {
		log.Printf("Error create table products with err: %s", err)
		helpers.NewHandlerResponse("Error create table products", nil).Failed(c)
	}
	helpers.NewHandlerResponse("Successfully migrate databases", nil).Success(c)
}
