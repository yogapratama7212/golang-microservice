package config

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

func Connect() *sql.DB {
	user := os.Getenv("DB_USERNAME")
	password := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")

	uri := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", user, password, host, port, dbname)
	db, err := sql.Open("mysql", uri)
	if err != nil {
		log.Fatalf("Can't connect to database with err: %s", err)
	}

	// defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err)
	}
	log.Println("Successfully connected to database")
	return db
}
