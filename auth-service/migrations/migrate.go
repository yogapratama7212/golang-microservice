package migrations

import (
	"log"

	"github.com/YogaPratama02/microservice-demo/auth-service/config"
	"github.com/YogaPratama02/microservice-demo/auth-service/helpers"
	"github.com/gin-gonic/gin"
)

func Migrate(c *gin.Context) {
	db := config.Connect()
	defer db.Close()

	sqlStatement := `CREATE TABLE users (
		id int AUTO_INCREMENT NOT NULL,
		name varchar(255) NOT NULL,
		email varchar(255) NOT NULL,
		password varchar(255) NOT NULL,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY(id),
		UNIQUE KEY(email)
	);`

	_, err := db.Exec(sqlStatement)

	if err != nil {
		log.Printf("Error create table products with err: %s", err)
		helpers.NewHandlerResponse("Error create table users", nil).Failed(c)
		return
	}
	helpers.NewHandlerResponse("Successfully migrate databases", nil).Success(c)
}
