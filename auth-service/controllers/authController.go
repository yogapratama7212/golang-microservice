package controllers

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/YogaPratama02/microservice-demo/auth-service/helpers"
	"github.com/YogaPratama02/microservice-demo/auth-service/models"
	"github.com/YogaPratama02/microservice-demo/auth-service/services"
	"github.com/gin-gonic/gin"
	"github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
)

type AuthController struct {
	service services.AuthService
}

func NewAuthController(service services.AuthService) *AuthController {
	return &AuthController{service}
}

func (authController *AuthController) RegisterController(c *gin.Context) {
	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		helpers.NewHandlerResponse(err.Error(), nil).BadRequest(c)
		return
	}

	if err := helpers.DoValidation(&user); err != nil {
		helpers.NewHandlerValidationResponse(err, nil).BadRequest(c)
		return
	}

	err := authController.service.Register(&user)
	if err != nil {
		sqlErr := err.(*mysql.MySQLError)
		if sqlErr.Number == 1062 {
			helpers.NewHandlerResponse(sqlErr.Message, nil).Failed(c)
			return
		}
		helpers.NewHandlerResponse(err.Error(), nil).Failed(c)
		return
	}
	helpers.NewHandlerResponse("Successfully register", nil).SuccessCreate(c)
}

func (authController *AuthController) LoginController(c *gin.Context) {
	var user models.UserLogin
	if err := c.ShouldBindJSON(&user); err != nil {
		helpers.NewHandlerResponse(err.Error(), nil).BadRequest(c)
		return
	}

	data, err := authController.service.Login(&user)
	if err != nil {
		helpers.NewHandlerResponse(err.Error(), nil).Failed(c)
		return
	}

	errHash := bcrypt.CompareHashAndPassword([]byte(data.Password), []byte(user.Password))
	if errHash != nil {
		fmt.Printf("Password Incorrect with err: %s\n", errHash)
		helpers.NewHandlerResponse("Password Incorrect", nil).BadRequest(c)
		return
	}

	tokenString, err := helpers.GenerateJWT(data)
	if err != nil {
		helpers.NewHandlerResponse(err.Error(), nil).Failed(c)
		c.Abort()
		return
	}

	id := strconv.Itoa(data.Id)

	http.SetCookie(c.Writer, &http.Cookie{
		Name:    "token",
		Value:   tokenString,
		Expires: time.Now().Add(30 * time.Hour),
		Path:    "/",
		// Local
		SameSite: 2,
		HttpOnly: true,
		// Domain:   "172.17.0.2",
	})
	http.SetCookie(c.Writer, &http.Cookie{
		Name:    "user_id",
		Value:   id,
		Expires: time.Now().Add(30 * time.Hour),
		Path:    "/",
		// Local
		SameSite: 2,
		HttpOnly: true,
		// Domain:   "172.17.0.2",
	})

	helpers.NewHandlerResponse("Successfully Login", nil).Success(c)
}

func (authController *AuthController) GetUserId(c *gin.Context) {
	userId, err := helpers.ParseJWTClaims(c)
	if err != nil {
		log.Printf("Error read claims with err: %s", err)
		helpers.NewHandlerResponse("Error read claims", nil).BadRequest(c)
		return
	}
	// c.JSON(200, userId)
	helpers.NewHandlerResponse("Successfully Get User Id", userId).Success(c)
}
