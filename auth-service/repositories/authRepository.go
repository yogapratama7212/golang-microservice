package repositories

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/YogaPratama02/microservice-demo/auth-service/models"
)

type AuthRepository interface {
	Register(user *models.User) error
	Login(user *models.UserLogin) (*models.UserLogin, error)
}

type authRepository struct {
	db *sql.DB
}

func NewAuthRepository(db *sql.DB) *authRepository {
	return &authRepository{db}
}

func (r *authRepository) Register(user *models.User) error {
	db := r.db
	sqlStatement := `INSERT INTO users (name, email, password, created_at, updated_at) VALUES (?, ?, ?, ?, ?)`
	_, err := db.Exec(sqlStatement, &user.Name, &user.Email, &user.Password, &user.CreatedAt, &user.UpdatedAt)
	if err != nil {
		log.Printf("Error create register user to database with err: %s", err)
		return err
	}
	return nil
}

func (r *authRepository) Login(user *models.UserLogin) (*models.UserLogin, error) {
	db := r.db
	userData := models.UserLogin{}
	sqlInfo := `SELECT id, password FROM users WHERE email = ?`
	err := db.QueryRow(sqlInfo, user.Email).Scan(&userData.Id, &userData.Password)
	if err != nil {
		log.Printf("Email not found with err: %s", err)
		return nil, fmt.Errorf("email not found")
	}

	return &userData, nil
}
