package services

import (
	"log"
	"time"

	"github.com/YogaPratama02/microservice-demo/auth-service/models"
	"github.com/YogaPratama02/microservice-demo/auth-service/repositories"
	"golang.org/x/crypto/bcrypt"
)

type AuthService interface {
	Register(user *models.User) error
	Login(user *models.UserLogin) (*models.UserLogin, error)
}

type authService struct {
	authRepository repositories.AuthRepository
}

func NewAuthService(repository repositories.AuthRepository) *authService {
	return &authService{repository}
}

func (s *authService) Register(user *models.User) error {
	hashPassword, _ := bcrypt.GenerateFromPassword([]byte(user.Password), 4)
	user.Password = string(hashPassword)
	userRegister := models.User{
		Name:      user.Name,
		Email:     user.Email,
		Password:  user.Password,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	if err := s.authRepository.Register(&userRegister); err != nil {
		log.Printf("Error register user to database with err: %s", err)
		return err
	}

	return nil
}

func (s *authService) Login(user *models.UserLogin) (*models.UserLogin, error) {
	userLogin := models.UserLogin{
		Email:    user.Email,
		Password: user.Password,
	}
	data, err := s.authRepository.Login(&userLogin)
	if err != nil {
		log.Printf("Error login to database with err: %s", err)
		return nil, err
	}

	return data, nil
}
