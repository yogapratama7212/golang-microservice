package routes

import (
	"fmt"
	"os"

	"github.com/YogaPratama02/microservice-demo/auth-service/config"
	"github.com/YogaPratama02/microservice-demo/auth-service/controllers"
	"github.com/YogaPratama02/microservice-demo/auth-service/migrations"
	"github.com/YogaPratama02/microservice-demo/auth-service/repositories"
	"github.com/YogaPratama02/microservice-demo/auth-service/services"
	"github.com/gin-gonic/gin"
)

func Init() {
	db := config.Connect()
	defer db.Close()

	// AUTH
	authRepository := repositories.NewAuthRepository(db)
	authService := services.NewAuthService(authRepository)
	authController := controllers.NewAuthController(authService)
	gin.SetMode(gin.ReleaseMode)
	e := gin.New()
	// e.LoadHTMLGlob("index.html")
	e.Use(gin.Recovery())
	v1 := e.Group("/api/v1")
	v1.Static("/public", "public/")
	v1.POST("/migrate", migrations.Migrate)
	v1.POST("/register", authController.RegisterController)
	v1.POST("/login", authController.LoginController)
	v1.GET("/get-user-id", authController.GetUserId)
	e.Run(fmt.Sprintf(":%s", os.Getenv("SERVER_PORT")))
}
